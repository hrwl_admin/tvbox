TVBox

本仓库管理者不能保证本仓库内容的合法性、准确性、完整性和有效性，请根据情况自行判断。本仓库内容，仅用于测试和学习研究，禁止用于商业用途，不得将其用于违反国家、地区、组织等的法律法规或相关规定的其他用途，禁止任何公众号、自媒体进行任何形式的转载、发布，请不要在中华人民共和国境内使用本仓库内容，否则后果自负。

本仓库内容中涉及的第三方硬件、软件等，与本仓库内容没有任何直接或间接的关系。本仓库内容仅对部署和使用过程进行客观描述，不代表支持使用任何第三方硬件、软件。使用任何第三方硬件、软件，所造成的一切后果由使用的个人或组织承担，与本仓库内容无关。

所有直接或间接使用本仓库内容的个人和组织，应 24 小时内完成学习和研究，并及时删除本仓库内容。如对本仓库内容的功能有需求，应自行开发相关功能。所有基于本仓库内容的源代码，进行的任何修改，为其他个人或组织的自发行为，与本仓库内容没有任何直接或间接的关系，所造成的一切后果亦与本仓库内容和本仓库管理者无关。

1. tvbox配置：

（1） jsm.json 来自js.json 修改 家庭电视可用 主要删除YouTube 直播。

（2）电视安卓版本低于4.4以下下载apk：<https://github.com/o0HalfLife0o/TVBoxOSC/releases/download/20230902-0114/TVBox_q215613905_20230902-0114.apk>


3. TVBox各路大佬配置（排名不分先后）：

（1）唐三：<https://gh.t4tv.hz.cz/newtang.bmp>

（2）Fongmi：<https://mirror.ghproxy.com/https://raw.githubusercontent.com/gaotianliuyun/fongmi/main/json/config.json>

（3）俊于：<http://home.jundie.top:81/top98.json>

（4）饭太硬：<http://饭太硬.top/tv>

（5）肥猫：<http://我不是.肥猫.love:63/接口禁止贩卖>

（6）霜辉月明py：<https://mirror.ghproxy.com/raw.githubusercontent.com/lm317379829/PyramidStore/pyramid/py.json>

（7）小米小爆脾气：<http://xhww.fun:63/小米/DEMO.json>

（8）小雅：<http://drpy.site/js1>

（9）菜妮丝：<https://tvbox.cainisi.cf>

（10）运输车：<https://github.moeyy.xyz/https://raw.githubusercontent.com/52670576/tvbox/main/ysc.json>

（11）多多：<https://yydsys.top/duo>

（12）南风：<https://agit.ai/Yoursmile7/TVBox/raw/branch/master/XC.json>

（13）神器：<https://神器每日推送.tk/pz.json>

（14）巧技：<http://pandown.pro/tvbox/tvbox.json>

（15）那里花开：<http://hz752.love:63/tk.json>

（16）吾爱有三：<http://52bsj.vip:98/0805>

（17）潇洒：<https://download.kstore.space/download/2863/01.txt>

（18）佰欣园：<https://mirror.ghproxy.com/https://raw.githubusercontent.com/chengxueli818913/maoTV/main/44.txt>

（19）胖虎：<https://notabug.org/imbig66/tv-spider-man/raw/master/配置/0801.json>

（20）云星日记：<http://itvbox.cc/tvbox/云星日记/1.m3u8>

（21）Yoursmile7：<https://agit.ai/Yoursmile7/TVBox/raw/branch/master/XC.json>

（22）Ray：<https://mirror.ghproxy.com/https://raw.githubusercontent.com/dxawi/0/main/0.json>

（23）甜蜜：<https://kebedd69.github.io/TVbox-interface/py甜蜜.json>

（24）月光宝盒：<https://jihulab.com/ygbh1/box/-/raw/main/月光宝盒>

（25）好人：<https://xhdwc.tk/0>

（26）电视（自用）： <https://gitee.com/hrwl_admin/tvbox/master/jsm.json>


自用仓库，如果喜欢，请Fork自用，谢谢！

尽自己所能更新，不保证配置的有效性和时效性。
